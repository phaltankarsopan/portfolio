// NAVBAR Resizing
var navbar = document.getElementById("main-nav");
var navbarLinks = document.querySelectorAll("nav a");

window.addEventListener("scroll", e => {
  // Display which section is active
  var scrollPosition = window.scrollY;
  navbarLinks.forEach(link => {
    var section = document.querySelector(link.hash);
    if (section.offsetTop <= scrollPosition + 320 &&
        section.offsetTop + section.offsetHeight > scrollPosition + 320) {
      link.classList.add("active");
    } else {
      link.classList.remove("active");
    }
  });

  // Resize the navbar after scrolling
  if (document.body.scrollTop > 45 || document.documentElement.scrollTop > 45) {
    navbar.style.padding = "1.2%";
    navbar.style.fontSize = "1em";
  } 
  else {
    navbar.style.padding = "1.7%";
    navbar.style.fontSize = "1.3em";
  }
})


// Carousal

let slideIndex = 1;
showSlide(slideIndex);

// Next/previous controls
function moveSlide(n) {
    showSlide(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlide(slideIndex = n);
}

function showSlide(n) {
  let i;
  let slides = document.getElementsByClassName("carousel");
  let dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
}

// MODAL
let modal1 = document.getElementById("modal1");
let col1 = document.getElementsByClassName("column")[0];
let modal2 = document.getElementById("modal2");
let col2 = document.getElementsByClassName("column")[1];
let modal3 = document.getElementById("modal3");
let col3 = document.getElementsByClassName("column")[2];
let modal4 = document.getElementById("modal4");
let col4 = document.getElementsByClassName("column")[3];
let modal5 = document.getElementById("modal4");
let col5 = document.getElementsByClassName("column")[4];

col1.onclick = function() {
  modal1.style.display = "block";
}

col2.onclick = function() {
  modal2.style.display = "block";
}

col3.onclick = function() {
  modal3.style.display = "block";
}

col4.onclick = function() {
  modal4.style.display = "block";
}

col5.onclick = function() {
  modal5.style.display = "block";
}

window.onclick = function(event) {
  if (event.target === modal1 || event.target === modal2 || event.target === modal3 || event.target == modal4 
    || event.target == modal5) {
    modal1.style.display = "none";
    modal2.style.display = "none";
    modal3.style.display = "none";
    modal4.style.display = "none";
  }
}